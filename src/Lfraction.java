/**
 * Kasutatud materjal:
 * http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
 * http://stackoverflow.com/questions/11302860/how-to-use-a-reduce-method-and-converting-improper-fractions-in-c-sharp-consol
 */

import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      //Tests
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b == 0)
         throw new ArithmeticException("Error: Divided by 0");
      else if (b > 0) {
         numerator = a;
         denominator = b;
      } else {
         numerator = -a;
         denominator = -b;
      }
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return numerator + "/" + denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      return (compareTo ((Lfraction)m) == 0);
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(numerator, denominator);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long numTemp = numerator * m.getDenominator() + denominator * m.getNumerator();
      long denTemp = denominator * m.getDenominator();
      return new Lfraction(numTemp, denTemp);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      if ((denominator == 0) || (m.denominator == 0))
         throw new IllegalArgumentException("Error: Invalid denominator");
      return new Lfraction(numerator * m.numerator, denominator * m.denominator).reduce();
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() throws ArithmeticException {
      if (numerator == 0)
         throw new ArithmeticException("Error: Denominator 0");
      return new Lfraction(denominator, numerator).reduce();
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      if (denominator == 0)
         throw new RuntimeException("Error: Denominator 0");
      return new Lfraction(-1 * numerator, denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      long numTemp = numerator * m.getDenominator() - denominator * m.getNumerator();
      long denTemp = denominator * m.getDenominator();
      if(denTemp == 0)
         throw new RuntimeException("Error: Denominator 0");
      return new Lfraction(numTemp, denTemp);
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) throws ArithmeticException {
      return times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo(Lfraction m) {
      long first = getNumerator() * m.getDenominator();
      long second = m.getNumerator() * getDenominator();
      int result = 0;
      if(first > second) {
         result = 1;
      }
      else if(second > first) {
         result = -1;
      }
      return result;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(getNumerator(), getDenominator());
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      Lfraction temp = reduce();
      return temp.getNumerator()/temp.getDenominator();
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction(numerator - denominator * integerPart(), denominator).reduce();
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return ((double)numerator) / ((double)denominator);
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      if (d > 0)
         return new Lfraction ((long)(Math.round (f * d)), d);
      else
         throw new ArithmeticException ("Denominator cannot be 0 or negative");
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      if(s == "")
         throw new IllegalArgumentException ("Field cannot be left empty");

      StringTokenizer strTemp = new StringTokenizer (s, "/<>[](),");
      int numTemp = 0;
      int denTemp = 1;
      if (strTemp.hasMoreTokens()) {
         numTemp = Integer.parseInt (strTemp.nextToken().trim());
      } else {
         throw new IllegalArgumentException (s + " is not a fraction");
      }
      if (strTemp.hasMoreTokens()) {
         denTemp = Integer.parseInt (strTemp.nextToken());
      } else {
         denTemp = 1;
      }
      if (strTemp.hasMoreTokens()) {
         throw new IllegalArgumentException (s + " is not a fraction");
      }
      return new Lfraction (numTemp, denTemp);
   }

   /** Reduce this fraction (and make denominator > 0).
    * @return reduced fraction
    */
   private Lfraction reduce()
   {
      long common = 0;
      long numTemp = Math.abs(numerator);
      long denTemp = Math.abs(denominator);
      if (numTemp > denTemp)
         common = gcd(numTemp, denTemp);
      else if (numTemp < denTemp)
         common = gcd(denTemp, numTemp);
      else
         common = numTemp;

      return new Lfraction(numerator/common, denominator/common);
   }

   /** Greatest common divisor of two given integers.
    * @param a first integer
    * @param b second integer
    * @return GCD(a,b)
    */
   private long gcd(long a, long b)
   {
      long denominator = Math.max(Math.abs(a), Math.abs(b));
      if (denominator == 0) throw new ArithmeticException("Denominator cannot be zero");
      long numerator = Math.min(Math.abs(a), Math.abs(b));
      while (numerator > 0) {
         a = denominator % numerator;
         denominator = numerator;
         numerator = a;
      }
      return denominator;
   }
}